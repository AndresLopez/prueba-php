<?php
$basedatos = "almacen";
$host ="localhost";
$user = "root";
$pass = "";

//conectamos con el servidor
$link = mysqli_connect($host, $user, $pass);

// comprobamos que hemos estabecido conexión en el servidor
if (! $link){
    echo "ERROR: Imposible establecer conección con el servidor" . PHP_EOL . "<br>";
    exit;
}else{
    //Intentamos establecer conexion con la db
    $conexion = mysqli_connect($host, $user,$pass,$basedatos);
    if (!$conexion){
        //Si falla es por que la db no existe, hay que crearla
        $create_db = "CREATE DATABASE $basedatos";
        if(!mysqli_query($link, $create_db)){
            echo "Error no se pudo crear la BD ". $basedatos;
            exit;
        }

        $conexion = mysqli_connect($host, $user,$pass,$basedatos);

        $create_table_1 = "CREATE TABLE marcas  (
            Id_marcas int NOT NULL AUTO_INCREMENT, 
            Nombre varchar(25) NOT NULL, 
            Descripcion varchar(100) NOT NULL, 
            PRIMARY KEY (Id_marcas)
        )ENGINE=InnoDB";
        
        if (!mysqli_query($conexion, $create_table_1)){
            echo "ERROR 1: Imposible crear la Tabla Marcas" . PHP_EOL . "<br>";
        }
        
        $solicitud2 = "CREATE TABLE producto (
            Id_producto int NOT NULL AUTO_INCREMENT,
            Nombre varchar(25) NOT NULL,
            Imagen MEDIUMBLOB NOT NULL ,
            Id_marcas int NOT NULL,
            PRIMARY KEY (Id_producto),
            FOREIGN KEY (Id_marcas) REFERENCES marcas(Id_marcas) ON DELETE RESTRICT ON UPDATE CASCADE
        )ENGINE=InnoDB";
        
        if (!mysqli_query($conexion, $solicitud2)){
            echo "ERROR 1: Imposible crear la Tabla Producto" . PHP_EOL . "<br>";
        }        
    }
}

?>


    
