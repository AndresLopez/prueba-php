<?php
//Header de la aplicacion
require('index_prod.php');
// CONEXION A LA BASE DE DATOS
include("../inc/conexion.php");
?>
<ul>
	<form method="POST" action="../inc/create_prod.php" enctype="multipart/form-data">
		Nombre del Producto:<input type="text" name="nombre" required><br><br>
        Marca del Producto: <select name="select" required>
            <?php
                //CONSULTADA
                $solicitud = "SELECT * FROM marcas";
                $resultado = mysqli_query($conexion, $solicitud);
                while($fila = mysqli_fetch_array($resultado) ){
                  echo "<option value=' " . $fila['Id_marcas'] . " '>" .$fila['Nombre']. "</option>"; 
                };
            ?> 
        </select> <br><br>
		Imagen del producto: <input type="file" name="imagen" accept="image/jpeg" require><br><br>
		<input type="submit" name="Enviar" value="Crear Producto">
	</form>
</ul>
<?php
//Footer de la aplicacion
require('footer.php');
?>