# Prueba PHP

## Introduccion

>Este proyecto es una prueba tecnica realizada en `PHP7`  `MySQL 5.5` y `Apache2.4` usandao `Vagrant` con `VirtualBox` sobre una imagen del sistema Operativo `CentOs7`

## Instalacion

> Para ejecutar la siguente aplicacion necesita tener instalado `Vagrant` y `VirtualBox`. Primero descarge el proyecto `prueba-php`,  a travez del siguiente comando.
>> `git clone https://gitlab.com/AndresLopez/prueba-php.git`

> Este repositorio no contiene ninguna clave para su descarga. De aqui puede observar la carpeta `src` que contiene todos los archivos necesarios para la ejecucion de la aplicacion. Es necesario que esta carpeta exista y su nombre no sea modificado porque sera utilizador por `Vagrant` para compartirla con la maquitna virtual.  

> Ejecute el archivo `Vagrantfie` a travez del siguiente comando
>>`vagrant up`

>Este comando descargar intalara y configurar la maquina virtual en su equipo.  la `IP` por defecto configurada para el proyecto es `192.168.1.100`. Ingrese la direcion `IP` y compruebe que el servidor `Apache` funciona correctamente. Para visualizar la  aplicacion ingrese la siguiente direccion:

>> `192.168.1.100/index.php`